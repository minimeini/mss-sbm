function [fig,stats,val] = plot_ri_compare(TrueSeq,HMMStSeqInd,KMStSeqInd,HMMStSeqMulti,KMStSeqMulti,type,task)
% function [fig] = plot_ri_compare(TrueSeq,HMMStSeqInd,KMStSeqInd,HMMStSeqMulti,KMStSeqMulti,type)
%
%
% >>> Input
%
% - TrueSeq: ntime x nsub matrix, true state sequence
% - HMMStSeqInd: ntime x nsub matrix, state sequence estimated by HMM 
% using subject-specific model
% - KMStSeqInd: ntime x nsub matrix, state sequence estimated by K-means 
% using subject-specific model
% - HMMStSeqMulti: ntime x nsub matrix, state sequence estimated by HMM
% using multi-subject model
% - KMStSeqMulti: ntime x nsub matrix, state sequence estimated by K-means
% using multi-subject model
% - type: {"ri","ari","nmi","nvi"}
% - task: {"lang","motor"}
%
% >>> Output
%
% - fig

cdir = fileparts(mfilename('fullpath'));
cdir = strsplit(cdir,'/');
cdir = strjoin(cdir(1:end-1),'/');
addpath(strcat(cdir,"/external/dyn_sbm"));

nsub = size(TrueSeq,2);

km_ind = zeros(1,nsub);
hmm_ind = zeros(1,nsub);
km_multi = zeros(1,nsub);
hmm_multi = zeros(1,nsub);
for s = 1:nsub
    if type == "ri"
        [~,hmm_ind(s),~,~] = valid_RandIndex(TrueSeq(:,s), HMMStSeqInd(:,s));
        [~,km_ind(s),~,~] = valid_RandIndex(TrueSeq(:,s), KMStSeqInd(:,s));
        [~,hmm_multi(s),~,~] = valid_RandIndex(TrueSeq(:,s), HMMStSeqMulti(:,s));
        [~,km_multi(s),~,~] = valid_RandIndex(TrueSeq(:,s), KMStSeqMulti(:,s));
    elseif type == "ari"
        hmm_ind(s) = valid_RandIndex(TrueSeq(:,s), ...
            HMMStSeqInd(:,s));
        km_ind(s) = valid_RandIndex(TrueSeq(:,s), ...
            KMStSeqInd(:,s));
        hmm_multi(s) = valid_RandIndex(TrueSeq(:,s), ...
            HMMStSeqMulti(:,s));
        km_multi(s) = valid_RandIndex(TrueSeq(:,s), ...
            KMStSeqMulti(:,s));
    elseif type == "nmi"
        hmm_ind(s) = getNMI(TrueSeq(:,s), HMMStSeqInd(:,s));
        km_ind(s) = getNMI(TrueSeq(:,s), KMStSeqInd(:,s));
        hmm_multi(s) = getNMI(TrueSeq(:,s), HMMStSeqMulti(:,s));
        km_multi(s) = getNMI(TrueSeq(:,s), KMStSeqMulti(:,s));
    elseif type == "nvi"
        hmm_ind(s) = nvi(TrueSeq(:,s), HMMStSeqInd(:,s));
        km_ind(s) = nvi(TrueSeq(:,s), KMStSeqInd(:,s));
        hmm_multi(s) = nvi(TrueSeq(:,s), HMMStSeqMulti(:,s));
        km_multi(s) = nvi(TrueSeq(:,s), KMStSeqMulti(:,s));
    elseif type == "fowlkes"
        hmm_ind(s) = fowlkes_mallows_index(TrueSeq(:,s),HMMStSeqInd(:,s));
        km_ind(s) = fowlkes_mallows_index(TrueSeq(:,s),KMStSeqInd(:,s));
        hmm_multi(s) = fowlkes_mallows_index(TrueSeq(:,s),HMMStSeqMulti(:,s));
        km_multi(s) = fowlkes_mallows_index(TrueSeq(:,s),KMStSeqMulti(:,s));
    elseif type == "mirkin"
        [~,~,hmm_ind(s),~] = valid_RandIndex(TrueSeq(:,s),HMMStSeqInd(:,s));
        [~,~,km_ind(s),~] = valid_RandIndex(TrueSeq(:,s),KMStSeqInd(:,s));
        [~,~,hmm_multi(s),~] = valid_RandIndex(TrueSeq(:,s),HMMStSeqMulti(:,s));
        [~,~,km_multi(s),~] = valid_RandIndex(TrueSeq(:,s),KMStSeqMulti(:,s));
    end
end

val = [hmm_ind;km_ind;hmm_multi;km_multi];
val = array2table(val','VariableNames',...
    {'HmmSingle','KmSingle','HmmMulti','KmMulti'});

stats = [mean(hmm_ind,'omitnan') sqrt(var(hmm_ind,'omitnan')) ...
    quantile(hmm_ind,[.025 .25 .50 .75 .975]);...
    ...
    mean(km_ind,'omitnan') sqrt(var(km_ind,'omitnan')) ...
    quantile(km_ind,[.025 .25 .50 .75 .975]);...
    ...
    mean(hmm_multi,'omitnan') sqrt(var(hmm_multi,'omitnan')) ...
    quantile(hmm_multi,[.025 .25 .50 .75 .975]);...
    ...
    mean(km_multi,'omitnan') sqrt(var(km_multi,'omitnan')) ...
    quantile(km_multi,[.025 .25 .50 .75 .975])];

stats = array2table(stats,'VariableNames',...
    {'mean','sd','q025','q25','q50','q75','q975'});
stats.Properties.RowNames = {'HmmSingle','KmSingle','HmmMulti','KmMulti'};
x1 = ([1.8,2.8]);
x2 = ([2.2,3.2]);
col = lines(2);

fig = figure;
ax1 = gca;

h1 = boxplot2([km_ind;km_multi]',x1,'barwidth', 0.1);
h2 = boxplot2([hmm_ind;hmm_multi]',x2,'barwidth', 0.1);

set(h1.box, 'color', col(2,:),'LineWidth',1.5);
set([h1.lwhis h1.uwhis], 'linestyle', '-', 'color', col(2,:),'LineWidth',1.5);
set(h1.med, 'color', col(2,:),'LineWidth',1.5);
set(h1.out, 'marker', '+', 'markeredgecolor', col(2,:));
set([h1.ladj h1.uadj], 'color', 'k','LineWidth',1.5);


set(h2.box, 'color', col(1,:),'LineWidth',1.5);
set([h2.lwhis h2.uwhis], 'linestyle', '-', 'color', col(1,:),'LineWidth',1.5);
set(h2.med, 'color', col(1,:),'LineWidth',1.5);
set(h2.out, 'marker', '+','markeredgecolor', col(1,:));
set([h2.ladj h2.uadj], 'color', 'k','LineWidth',1.5);

ax1.FontSize = 14;
% ylim(ax1,[0.45 0.8]);
legend([h1.box(1) h2.box(1)],{'K-means' 'MSS-SBM'},...
    'Location','southeast');
xticks([2 3]); xticklabels({'Subject-Specific','Multi-Subject'});

box(ax1,'on');
if type == "ri"
    ylabel('Rand Index');
elseif type == "ari"
    ylabel('Adjusted Rand Index');
elseif type == "nmi"
    ylabel('Normalized Mutual Information');
elseif type == "nvi"
    ylabel('Normalized Variation Information');
elseif type == "fowlkes"
    ylabel("Fowlkes-Mallows Index");
elseif type == "mirkin"
    ylabel("Mirkin Index");
else
    ylabel('Performance');
end

if task=="lang"
    title('Language fMRI');
    xlabel('(a)');
elseif task=="motor"
    title('Motor fMRI');
    xlabel('(b)');
end


end