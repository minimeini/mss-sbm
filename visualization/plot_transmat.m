function fig = plot_transmat(TransmatCell,BestPermHMM,stat,type)
% function fig = plot_transmat(TransmatCell,stat,type)
%
% - TransmatCell
% - BestPermHMM: nstate x nsub

nsub = length(TransmatCell);
nstate = size(TransmatCell{1},1);
transmat = zeros(nstate,nstate,nsub);
for s = 1:nsub
    transmat(:,:,s) = TransmatCell{s}(BestPermHMM(:,s),BestPermHMM(:,s));
end

if nstate == 2
    labs = {'Story','Math'};
elseif nstate == 6
    labs = {'cue','right-hand','right-foot',...
        'left-hand','left-foot','tongue'};
end

if type == "point"
    if stat == "mean"
        est = mean(transmat,3);
    elseif stat == "median"
        est = median(transmat,3);
    end
    fig = figure;
    imagesc(est);
    xticks(1:nstate);
    xticklabels(labs);
    xtickangle(90);
    yticks(1:nstate);
    yticklabels(labs);
    axis('square');
    colorbar;
elseif type == "interval"
    if stat == "mean"
        est = cat(3,mean(transmat,3)-2*std(transmat,0,3),...
            mean(transmat,3),...
            mean(transmat,3)+2*std(transmat,0,3));
        tabs = {'Lower Bound', 'Mean', 'Upper Bound'};
    elseif stat == "median"
        est = quantile(transmat,[0.025,0.50,0.975],3);
        tabs = {'Lower Bound', 'Median', 'Upper Bound'};
    end
    fig = figure;
    for i = 1:3
        subplot(1,3,i);
        imagesc(est(:,:,i));
        xticks(1:nstate);
        xticklabels(labs);
        xtickangle(90);
        yticks(1:nstate);
        yticklabels(labs);
        title(tabs{i});
        axis('square');
        
        ax = gca;
        ax.YAxis.FontSize = 6;
        ax.XAxis.FontSize = 6;
    end
    colorbar;
    suptitle("");
end

end