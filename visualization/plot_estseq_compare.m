function [f,BestPermHMM] = plot_estseq_compare(TrueSeq,KMStSeq,HMMStSeq,varargin)

% TrueSeq: ntime x nsub
% KMStSeq: ntime x nsub
% HMMStSeq: ntime x nsub

nsub = size(TrueSeq,2);
nelem = size(TrueSeq,1);
nstate = unique(TrueSeq(:));

%% Postprocess Estimated Sequence

if nargin > 3
    BestPermHMM = varargin{1};
else
    [HMMStSeq,BestPermHMM] = permute_estseq(TrueSeq,HMMStSeq);
    [KMStSeq,BestPermKM] = permute_estseq(TrueSeq,KMStSeq);
end

%%
hmm_ri = zeros(1,nsub);
for s = 1:nsub
    hmm_ri(s) = valid_RandIndex(TrueSeq(:,s), HMMStSeq(:,s));
end
[~,sort_idx] = sort(hmm_ri,'descend');

TrueSeq = TrueSeq(:,sort_idx)';
KMStSeq = KMStSeq';
% KMStSeq = KMStSeq(:,sort_idx)';
HMMStSeq = HMMStSeq(:,sort_idx)';
%% Plot Estimated Sequence
f = figure;

% Position: [left bottom width height]
h1 = subplot(3,1,1);
imagesc(uint8(TrueSeq));
title("Ground Truth");
h1.FontSize = 12;

if numel(nstate) == 2
    cmap = [225 225 204; 58 70 96] / 225;
    colormap(cmap);
elseif numel(nstate) == 6
    cmap = [252 255 195;...
        151 103 193; ...
        0 159 151; ...
        255 0 46; ...
        54 70 97; ...
        140 203 254] ./ 255;
    colormap(cmap);
end

c = colorbar(h1,'Location','northoutside');
c.FontSize = 12;

if numel(nstate) == 2
    c.Ticks =([1,2]);
    c.TickLabels = ({'Story','Math'});
elseif numel(nstate) == 6
    c.Ticks =([1,2,3,4,5,6]);
    c.TickLabels = ({'cue','right-hand','right-foot',...
        'left-hand','left-foot','tongue'});
end

h2 = subplot(3,1,2);
imagesc(uint8(KMStSeq));
title("K-means Estimates");
h2.FontSize = 12;
ylabel('Subject');

h3 = subplot(3,1,3);
imagesc(uint8(HMMStSeq));
title("HMM Estimates");
h3.FontSize = 12;
xlabel('Time Point');



end