function fig = plot_modular_probmat(NodeConn,AxisLab,align,varargin)

dataset = 'simulation';
if nargin > 3
    dataset = varargin{1};
end

fig = figure;
minval = min(cat(3,NodeConn{:}),[],'all','omitnan');
maxval = max(cat(3,NodeConn{:}),[],'all','omitnan');
nstate = numel(NodeConn);
    
nrow = floor(nstate/2);
ncol = ceil(nstate/nrow);

if ~isequal(dataset,'simulation') && nstate == 2
    labs = {'Story','Math'};
elseif ~isequal(dataset,'simulation') && nstate == 6
    labs = {'cue','right-hand','right-foot',...
        'left-hand','left-foot','tongue'};
else
    labs = arrayfun(@(x)("State "+num2str(x)),1:nstate,...
        'UniformOutput',false);
end

for i = 1:nstate
    if nstate == 2
        if align == "none" || align == "col"
            subplot(1,2,i);
        elseif align == "row"
            subplot(2,1,i);
        end
    elseif nstate == 6
        if align == "none"
            subplot(2,3,i);
        elseif align == "row"
            subplot(6,1,i);
        elseif align == "col"
            subplot(1,6,i);
        end
    else
        subplot(nrow,ncol,i);
    end
    imagesc(NodeConn{i},[minval maxval]);    
    title(labs{i});

    if ~isempty(AxisLab)
        xticks(1:numel(AxisLab));
        xtickangle(90);
        xticklabels(AxisLab);
        yticks(1:numel(AxisLab));
        yticklabels(AxisLab);
    end
    axis('square');
    
    ax = gca;
    ax.YAxis.FontSize = 3;
    ax.XAxis.FontSize = 3;
end

colormap(jet);
if align == "none"
    colorbar;
elseif align == "row"
    colorbar('southoutside');
elseif align == "col"
    colorbar('eastoutside');
end
suptitle("");

end