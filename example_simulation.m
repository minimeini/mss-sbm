close all; clear all; clc;
%% Dependencies
addpath('./external/hmmcodes');
addpath('./external/dyn_sbm');
addpath('./external/GenLouvain');
addpath('./simulation');
addpath('./community_detection');
addpath('./hidden_states');
addpath('./visualization');

%% Simulation Setting
S = 3; % number of states
Srep = 2; % repeat each state `Srep=2` times
inseq = 1; % If inseq = 1, state sequence will be 1-2-3-1-2-3; Otherwise, it will be random.
T = 240; % number of observations/time points
K = 5; % number of communities
N = 120; % number of nodes/regions/ROIs
lambda = [0.9 0.75 0.6];

NK = numel(K);
R = 5; % number of subjects
Q = 10; % number of replications per subject

%% Model Setting
% CommunityDetection: algorithm used for community detection.
% - 'MultiLayer': multi-layer community detection
% - 'Known': use true community membership
CommunityDetection = 'MultiLayer';

%% Run Simulation
[g, real_states, Theta_time_true, Theta_state_true, W, Waver, ...
    LouvainSeq, Ql,hmm_flag,hmm_iter,hmm_ll,hmm_ari,hmm_mse,...
    hmm_states,hmm_mu,hmm_runtime] = ...
    main_simulation_sbm('S',S,'Srep',Srep,'inseq',inseq,...
    'T',T,'K',K,'N',N,'R',R,'Q',Q,'lambda',lambda,...
    'CommunityDetection', CommunityDetection);

hmm_states = permute(hmm_states,[2 1 3]);
hmm_states = reshape(hmm_states,T,[]);
for i = 1:size(hmm_states,2)
    [hmm_states(:,i),BestPermHMM(:,i)] = ...
        permuteClasses(real_states,hmm_states(:,i)');
end

hmm_mu = reshape(hmm_mu,size(hmm_mu,1),size(hmm_mu,2),[]);
%%
blue = [0 0.4470 0.7410];
red = [0.8500 0.3250 0.0980];
yellow = [0.9290 0.6940 0.1250];

%% Estimated community membership
figure;
h1 = subplot(2,1,1);
imagesc(uint8(g)');
h1.YTickMode = 'manual';
h1.YTick = [];
ylabel("");
title("True Community Membership");
h1.FontSize = 12;

c = colorbar(h1,'Location','northoutside');
c.FontSize = 12;
c.Ticks = (1:1:K);
c.TickLabels = (arrayfun(@(x)("Community "+num2str(x)),...
    1:K,'UniformOutput',false));

h2 = subplot(2,1,2);
imagesc(LouvainSeq');
title('Estimated Community Membership');
xlabel("Nodes");
ylabel("Subjects");
h2.FontSize = 12;
%% Estimated hidden states
figure;
h1 = subplot(2,1,1);
imagesc(uint8(real_states));
h1.YTickMode = 'manual';
h1.YTick = [];
ylabel("");
title("True state sequence");
h1.FontSize = 12;
    
cmap = [252 255 195; 151 103 193; 0 159 151] ./ 255;
colormap(cmap);
c = colorbar(h1,'Location','northoutside');
c.FontSize = 12;
c.Ticks = (1:1:S);
c.TickLabels = (arrayfun(@(x)("State "+num2str(x)),...
    1:S,'UniformOutput',false));

[~,sort_idx] = sort(hmm_ari(:),'descend');
h2 = subplot(2,1,2);
imagesc(hmm_states(:,sort_idx)');
title('HMM Estimation');
xlabel("Time Points");
ylabel("Subjects");
h2.FontSize = 12;

%% Estimated Modular Connectivity Probability Matrices
try
    clear hmm_mu_cell
catch
end

for i = 1:size(hmm_mu,3)
    hmm_mu_cell{i} = hmm_mu(:,:,i);
end
CommEst = LouvainSeq(:,1);
ncomm_max = numel(unique(CommEst(:)));
NodeName = arrayfun(@num2str,1:N,'UniformOutput',false);
[~,NodeConnEst,~] = get_modular_probmat(CommEst,hmm_mu_cell,...
    BestPermHMM,ncomm_max,"mean",NodeName);

minval = min(reshape(cat(3,NodeConnEst{:}),1,[]),[],'all','omitnan');
maxval = max(reshape(cat(3,NodeConnEst{:}),1,[]),[],'all','omitnan');
labs = arrayfun(@(x)("State "+num2str(x)),1:S,...
    'UniformOutput',false);

figure;
for i = 1:S
    subplot(1,S,i);
    imagesc(NodeConnEst{i},[minval maxval]);    
    title(labs{i});
    
    if i==1, ylabel("Nodes"); end
    xlabel("Nodes");
    axis('square');
    set(gca,'FontSize',12);
end

colormap(jet);
colorbar('Location','eastoutside');
suptitle("Estimated Modular Connection Probability Matrices");
