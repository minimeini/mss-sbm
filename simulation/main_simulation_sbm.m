function [g, real_states, Theta_time_true, Theta_state_true, W, Waver, ...
    LouvainSeq, Ql,hmm_flag,hmm_iter,hmm_ll,hmm_ari,hmm_mse,...
    hmm_states,hmm_mu,hmm_runtime] = ...
    main_simulation_sbm(varargin)

%function [] = main_simulation_sbm(varargin)
%% 1.1 Global Settings

% S = 2; % number of states
% Srep = 2; % repeat every state `Srep` times
% inseq = true; % bool, TRUE: ABABAB, FALSE: randomized
% 
% K = 2:2:12; % number of comunities
% N = 120; % number of nodes in network 
% T = 200; % number of time points
% 
% R = 50; % number of subjects/layes (was "P")
% Q = 100; % number of iterations for each subject/layer
% 
% alpha = 1; % network sparsity under different states
% lambda = {[0.9 0.6]};

%% References
%{
Jing, L. E. I., & Rinaldo, A. (2015). Consistency of spectral clustering 
in stochastic block models. Annals of Statistics, 43(1), 215?237. 
%}


%% Notation and Symbols
%{
N: 1x1 numeric scalar, # of ROIs
R: 1x1 numeric scalar, # of subjects
K: 1x1 numeric scalar, # of communities == # of classes in SBM
S: 1x1 numeric scalar, # of states
T: 1x1 numeric scalar, # of time points

Srep: 1x1 numeric scalar, repeat the states `StateSet` for `rep` times

real_states[t]: 1x1 numeric, state at time point t, 1<=t<=T
ConnRoi[t]: NxN numeric or binary (?) matrix, 1<=t<=T
ConnCom[t]: KxK numeric or binary? matrix,  1<=t<=T
ComMat: NxK binary matrix, the community membership
ComVec: Nx1 integer vector, the community membership
%}

%{
conn: short for `connectivity`
com: short for `community(ies)`
%}

% AUTHOR: Balqis
% EDITED: Meini

%% Model Framework
% > Rank reduction: from ROI space to community space, the communites are
% controlled by hidden states.

% MODEL STRUCTURE
%{
1. Community Structure: remains unchanged during the whole time series.

    - commat_idx: NxK binary matrix indicates community memberships

2. State Space Model (HMM)

    - state[t]|state[t-1] ~ Multinomial with K different states

    - conn_com[t] ~ Normal(mu[state[t]], sigma^2[state[t]])

3. Stochastic Block Model
    - sigma[t] = commat_idx * conn_com[t] * commat_idx'
    - conn_roi[t] ~ Bernouli(sigma[t])
%}

%% Simulation Scheme
%{
	1. PRESPECIFIED: S, N, T, StateSet, rep

	- SIMULATION: Bottom -> Up

        2. [deterministic] Generate state time series `state` with
        certein number of states `S` and given `T.` 

        3. [deterministic] Set the number of communities, which 
            remains the same across subjects. Assign community membership 
            `comvec_idx` Naive membership vector
                - Membership index set `{1,2,...,K}`.
                - Each index is repeated `floor(N/K)` times
                - If NN=length(comvec_idx) < N, sample N-NN from Unif[1 K]
                - create `commat_idx` from `comvec_idx`

		- Step 4. [random] Simulate `conn_com[t]`: (forward HMM)
            - get the upper triangle part: conn_com_tri[s] ~ unif[0, 0.2]
            - get the diagonal part: conn_com_diag[s] ~ unif[a[s], b[s]], 
              where b[s] - a[s] == 0.2
            - conn_com_bystate[s] = conn_com_tri[s] + conn_com_trutri[s]' - 
              diag(conn_com_tri) + conn_com_diag, 
              conn_com_bystate: [KxKxS]
            - conn_com[t] = conn_com_bystate[state[t]], 
              conn_com: [KxKxT]

        - Step 5. [random] Simulate `conn_roi[t]` (SBM)
            - sigma[t] = commat_idx * conn_com[t] * commat_idx', [NxN]
            - conn_roi[i,j,t] ~ bernouli(sigma[i,j,t])
            - Postprocess: set the self-connection to zero, ensure symmetry


    - GO THROUGH METHODOLOGY: Top -> Down

        - Step 1. Stochastic Block Model: 
            - ROI connectivity + membership => community connectivity

        - Step 2. Estimate Hidden States from the community space time
        series. Proposed Model: (backward) HMM, benchmark: k-means
        
        - Step 3. Performance comparison. 
            Metric: Rand Index (states), Mean Square Error (estimated
            community)

        - Step 4. Visualization
            - Rand Index: x=K, y=ri, group by=S
            - MSE: x=K, y=MSE, group by = states, a plot for each S
%}

% Parse inputs
p = inputParser;
addParameter(p, 'S', 3, @isnumeric);
addParameter(p, 'Srep', 2, @isnumeric);
addParameter(p, 'inseq', 1, @isnumeric);
addParameter(p, 'K', 2:2:12, @isnumeric);
addParameter(p, 'N', 120, @isnumeric);
addParameter(p, 'T', 240, @isnumeric);
addParameter(p, 'R', 50, @isnumeric);
addParameter(p, 'Q', 100, @isnumeric);
addParameter(p, 'alpha', 0.8, @isnumeric); % control sparsity:low alpha=sparse
addParameter(p, 'lambda', [0.9 0.75 0.6], @isnumeric);
addParameter(p, 'gamma', 1, @isnumeric);
addParameter(p, 'omega', 0.5, @isnumeric);
addParameter(p, 'var', 0.05, @isnumeric); % noise variance: increase
addParameter(p, 'nmix', 1, @isnumeric);
addParameter(p, 'nwait', 300, @isnumeric);
addParameter(p, 'cov_type', 'full', @ischar);
addParameter(p, 'CommunityDetection', 'MultiLayer', @ischar); % {'MultiLayer', 'SingleLayer', 'Known'}
addParameter(p, 'verbose', false, @islogical);
addParameter(p, 'savepath', [], @ischar);

parse(p,varargin{:});
S = p.Results.S;
Srep = p.Results.Srep;
inseq = p.Results.inseq;
K = p.Results.K;
N = p.Results.N;
T = p.Results.T;
R = p.Results.R;
Q = p.Results.Q;
alpha = p.Results.alpha;
lambda = p.Results.lambda;
gamma = p.Results.gamma;
omega = p.Results.omega;
var = p.Results.var;
nmix = p.Results.nmix;
nwait = p.Results.nwait;

savepath = p.Results.savepath;
if exist(savepath, 'dir') == 0
    mkdir(savepath);
end

cov_type = p.Results.cov_type;
CommunityDetection = p.Results.CommunityDetection;
verbose = p.Results.verbose;

%% Load Dependencies
if isempty(gcp('nocreate'))
    parpool();
end

cdir = fileparts(mfilename('fullpath'));
cdir = strsplit(cdir,'/');
cdir = strjoin(cdir(1:end-1),'/');
addpath(strcat(cdir,'/external/dyn_sbm'));
addpath(strcat(cdir,'/external/hmmcodes'));
addpath(strcat(cdir,'/external/GenLouvain'));
addpath(strcat(cdir,'/hidden_states'));

warning('off','all');
%% 1.2 Generate state time series and transition matrix among states
% S: interger, the number of states
% Srep: integer, repeat every state `srep` times
% inseq: bool, TRUE: ABABAB, FALSE: randomized

NK = numel(K);
transmat0 = get_default_transmat(S,inseq); 
% transmat0: initial guess of transition matrix for HMM
    
%% loop through the number of communities
for ik = 1:NK % number of communities
    k = K(ik);
    %%

    hmm_flag = zeros(R,Q);
    hmm_iter = zeros(R,Q);
    hmm_ll = zeros(R,Q);
    hmm_ari = zeros(R,Q);
    hmm_mse = zeros(R,S,Q);
    hmm_states = zeros(R,T,Q);
    hmm_runtime = zeros(R,Q);
    
    % Generate simulated data
    [real_states,g,Theta_state_true,Theta_time_true,W,Waver] = ...
        generateSimData(K,N,T,R,S,Srep,inseq,var,lambda,alpha);
    ThetaStateVec = zeros(K*(K+1)/2, S);
    for s = 1:S % vectorise matrix
        ThetaStateVec(:,s) = blockmat2vec(Theta_state_true(:,:,s),false);
    end
    
    fprintf('>>>>> Generate Simulated Data.\n');

    % Community membership estimation
    if isequal(CommunityDetection, 'MultiLayer')
        [LouvainSeq,Ql] = multilayer_Qmax(gamma,omega,N,Waver);
    elseif isequal(CommunityDetection, 'Known')
        Ql = 'Not run';
        LouvainSeq = g;
    end
    
    Kest = numel(unique(LouvainSeq(:)));
    hmm_mu = zeros((Kest+1)*Kest/2,S,Q,R);
    
    fprintf('>>>>> Community Detection: %s with %d communities.\n',...
        CommunityDetection,numel(unique(LouvainSeq(:))));
    
    membership = LouvainSeq(:,1)';
    %%
    parfor r = 1:R % r - index of subject
        %%
        tStart=tic;
        [hmm_flag(r,:),hmm_iter(r,:),hmm_ll(r,:),hmm_ari(r,:),hmm_mse(r,:,:),...
            hmm_states(r,:,:),hmm_mu(:,:,:,r),hmm_runtime(r,:)] = ...
            est_dyn_states(T,S,Q,real_states,membership,W(:,:,:,r),...
            Theta_state_true,transmat0,nmix,nwait,cov_type,verbose);
        tElapsed=toc(tStart);
        fprintf('>>>>> HMM - K=%d Subject %d: %fs.\n',k,r,tElapsed);
    end % end loop over subjects
    
    %%
    if ~isempty(savepath) || NK>1
        filename = [savepath '/simout_state' num2str(S) '_rep' ...
            num2str(Srep) '_inseq' num2str(inseq) '_comm' ...
            num2str(k) '.mat'];
        save(filename, 'g','real_states', 'Theta_time_true', ...
            'Theta_state_true', 'W', 'Waver', 'LouvainSeq', 'Ql',...
            'hmm_flag','hmm_iter','hmm_ll','hmm_ari',...
            'hmm_mse','hmm_states','hmm_mu','hmm_runtime');
    end
    
end % end for number of communities

delete(gcp('nocreate'));
end
