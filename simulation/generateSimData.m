function [real_states,g,ThetaByState,ThetaByTime,W,Waver] = generateSimData(K,N,T,R,S,Srep,inseq,noiseVar,lambda,alpha)
if isempty(gcp('nocreate'))
    parpool();
end
logistic = @(x) 1./(1+exp(-x)); %logistic function

real_states = generate_state_series(S, T, Srep, inseq);
g = generate_membership(N,K);

ThetaByState = zeros(K,K,S);
for s = 1:S
    ThetaByState(:,:,s) = generate_modular_connectivity(K,...
        lambda(s),alpha);
end

ThetaByTime = zeros(K,K,T);
parfor t = 1:T
    noise = randn(K) .* sqrt(noiseVar);
    noise = logistic(triu(noise) + triu(noise)');
    ThetaByTime(:,:,t) = ThetaByState(:,:,real_states(t)) + noise;
end
ThetaByTime(ThetaByTime>1) = 1;


W = zeros(N,N,T,R);
Waver = zeros(N,N,R);
for r = 1:R
    parfor t = 1:T
        % Generate subject-specific adjacency matrix
        W(:,:,t,r) = generateSbm(g, ThetaByTime(:,:,t));
    end
    Waver(:,:,r) = generateSbm(g, mean(ThetaByTime,3));
end

end