function g = generate_membership(N,K)

n_k = round(N/K); % number of nodes per community
for k=1:K
    g(n_k*(k-1)+1:n_k*(k-1)+n_k,1) = k;
end

% g = reshape(repmat(1:K, n_k, 1), [K*n_K,1]);
    
if numel(g) > N
    g = g(1:N, 1);
else
    g = [g; randi([1 K], [N-numel(g),1])];
end

end