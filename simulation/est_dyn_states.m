function [hmm_flag,hmm_iter,hmm_ll,hmm_ari,hmm_mse,...
    hmm_states,hmm_mu,hmm_runtime] = ...
    est_dyn_states(T,S,Q,real_states,g,W,ThetaStateVec,...
    transmat0,nmix,nwait,cov_type,verbose)

%%
logit = @(x) log(x)-log(1-x);
K = numel(unique(g(:)));

hmm_iter = zeros(1,Q);
hmm_flag = zeros(1,Q);
hmm_ll = zeros(1,Q);

hmm_ari = zeros(1,Q);
hmm_mse = zeros(S,Q);
hmm_states = zeros(T,Q);
hmm_mu = zeros((K+1)*K/2,S,Q);
hmm_runtime = zeros(1,Q);

Theta_est = zeros((K+1)*K/2, T, Q);
q = 0;
while q < Q % Begin Iteration.
    q = q + 1;

    for t = 1:T
        % 2.1 [random] Estimate community connectivity `Theta_est` using SBM
        [Theta_est(:,t,q),~,~] = estimateSbmProb(W(:,:,t),g,[]);
    end
    Theta_est_odds = logit(Theta_est(:,:,q));
    
    
%     try
        %% 2.2 Estimate hidden states using HMM
        tstart = tic;
        [est_states,~,hmm_ll(q),mse,hmm_flag(q),hmm_iter(q),...
            transmat1,hmm_mu(:,:,q),~,hmm_runtime_tmp] = ...
            run_hmm(Theta_est_odds,ThetaStateVec,S,transmat0,...
            nmix,nwait,cov_type,verbose,real_states);
        
        hmm_runtime(q) = sum(hmm_runtime_tmp(:));

        hmm_states(:,q) = est_states(:);
        hmm_ari(q) = valid_RandIndex(real_states(:),est_states(:));
        hmm_mse(:,q) = mse(:);

        telapsed = toc(tstart);
        if verbose
            fprintf(">>> Time: %.2fs, HMM LL: %.4f.\n",telapsed,hmm_ll(q));
        end
        
%     catch
%         q = q - 1;
%         continue;
%     end
    

end % end for iterations for a subject

if any(hmm_flag == 0)
    warning('simulate_single_subject: unreliable results with exit=0.');
end
end