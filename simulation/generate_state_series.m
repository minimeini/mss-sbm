function state_series = generate_state_series(S, T, Srep, inseq)
% function state_series = generate_state_series(S, T, Srep, inseq)
%
% Generate a time series of the hidden states.
%
%
% >>> INPUT
%
% - S: the number of states.
% - T: the number of time points.
% - Srep: integer vector with length >=1 and <=S, repeat every s in S srep
% in Srep times.
% - inseq: bool, if the state sequence is [S1,S2,...] or random.
%
%
% >>> OUTPUT
%
% - state_series: integer vector with length=T.
%
%
% Meini, Jan 16, 2020.

if nargin < 4
    inseq = 1;
end

if numel(Srep) < S
    Srep = [Srep repmat(Srep(end), 1, S-numel(Srep))];
end

t = floor(T/sum(Srep));

mins = min(Srep);
state_sequence = repmat(1:S, 1, mins);
for s  = 1:S
    if Srep(s) > mins
        state_sequence = [state_sequence repmat(s,1,Srep(s)-mins)];
    end
end

state_series = repmat(state_sequence,t,1);

if inseq == 0 % out of sequence state
    new_idx = randperm(sum(Srep));
    state_series = state_series(:,new_idx);
end

state_series = reshape(state_series,1,[]);
if numel(state_series) < T
    state_series = [state_series ...
        ones(1,T-numel(state_series)) * state_series(end)];
end


end