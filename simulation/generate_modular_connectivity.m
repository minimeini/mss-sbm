function P = generate_modular_connectivity(K,lambda,varargin)

nrequired = 2;
alpha = 1; if nargin > nrequired, alpha = varargin{1}; end
a = -0.1; if nargin > nrequired+1, a = varargin{2}; end
b = 0.1; if nargin > nrequired+2, b = varargin{3}; end

% Synthetic density matrix
pr = a + (b-a)*rand(1,1); % Subject-specific random deviation in block probability
pii = lambda + pr; % within-block connection probability
% pij = (1 - pii)/(K-1); % between-block connection probability
% P(:,:) = pii.*eye(K) + pij.*ones(K) - pij.*eye(K);
pij = 1 - pii;
P(:,:) = pii.*eye(K) + pij.*ones(K);
P = alpha.*P;

end