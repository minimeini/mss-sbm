clear all; clc;

S = 3;
Srep = 2;
inseq = 1;
T = 240;
K = 2:2:12; 
lambda = [0.9 0.75 0.6];

NK = numel(K);
R = 50;
Q = 100;

real_states = generate_state_series(S, T, Srep, inseq);

out = struct('hmm_ll',[], 'hmm_ri',[],...
    'hmm_ari',[],'hmm_mse',[],'hmm_states',[],...
    'km_states',[],'km_ri',[],...
    'km_ari',[],'km_mse',[]);

%%
addpath('../external/dyn_sbm');

%%
for ik = 1:NK
    tmp = load(['../output/simout_state' num2str(S) '_rep' num2str(Srep) ...
        '_inseq' num2str(inseq) '_comm' num2str(K(ik)) '_new.mat']);
    
    out.hmm_ll{ik} = tmp.hmm_ll;
    out.hmm_ri{ik} = tmp.hmm_ri;
    out.hmm_ari{ik} = tmp.hmm_ari;
    out.hmm_mse{ik} = tmp.hmm_mse;
    out.hmm_states{ik} = permute(tmp.hmm_states,[2 1 3]);
    
    out.km_ri{ik} = tmp.km_ri;
    out.km_ari{ik} = tmp.km_ari;
    out.km_mse{ik} = tmp.km_mse;
    out.km_states{ik} = permute(tmp.km_states,[2 1 3]);
    
    if any(isnan(reshape(out.hmm_ll{ik},1,[]))) || ...
            any(isinf(out.hmm_mse{ik}(:)))
        [rows,cols,~] = find(isnan(out.hmm_ll{ik}));
        [rows2,cols2,~] = find(isinf(out.hmm_mse{ik}));
        
        rows = [rows(:) rows2(:)];
        cols = [cols(:) cols2(:)];
        out.hmm_ll{ik}(rows,:) = [];
        out.hmm_ri{ik}(rows,:) = [];
        out.hmm_ari{ik}(rows,:) = [];
        out.hmm_mse{ik}(rows,:,:) = [];
        out.hmm_states{ik}(:,rows,:) = [];
        
        out.km_ri{ik}(rows,:) = [];
        out.km_ari{ik}(rows,:) = [];
        out.km_mse{ik}(rows,:,:) = [];
        out.km_states{ik}(:,rows,:) = [];
    end
    
    out.hmm_ri{ik} = squeeze(mean( out.hmm_ri{ik}(1:min(50,size(out.hmm_ri{ik},1)),:),2,'omitnan' ));
    out.hmm_ari{ik} = squeeze(mean(out.hmm_ari{ik}(1:min(50,size(out.hmm_ari{ik},1)),:),2,'omitnan'));
    out.hmm_mse{ik} = squeeze(mean(out.hmm_mse{ik}(1:min(50,size(out.hmm_mse{ik},1)),:,:),3,'omitnan'));
    out.hmm_states{ik} = out.hmm_states{ik}(:,1:min(50,size(out.hmm_states{ik},2)),:);
    
    out.km_ri{ik} = squeeze(mean(out.km_ri{ik}(1:min(50,size(out.km_ri{ik},1)),:),2,'omitnan'));
    out.km_ari{ik} = squeeze(mean(out.km_ari{ik}(1:min(50,size(out.km_ari{ik},1)),:),2,'omitnan'));
    out.km_mse{ik} = squeeze(mean(out.km_mse{ik}(1:min(50,size(out.km_mse{ik},1)),:),3,'omitnan'));
    out.km_states{ik} = out.km_states{ik}(:,1:min(50,size(out.hmm_states{ik},2)),:);
    clear tmp;
end


blue = [0 0.4470 0.7410];
red = [0.8500 0.3250 0.0980];
yellow = [0.9290 0.6940 0.1250];

%% 1 Rand index with confidence interval across varying #of communities

% calculate stats
hmm_ri_stat = zeros(3,numel(K)); % mean, lower, up
km_ri_stat = zeros(3,numel(K)); % mean, lower, up

for ik = 1:numel(K)
    ri = out.hmm_ari{ik};
    hmm_ri_stat(1,ik) = mean(ri);
    sem = std(ri) / sqrt(numel(ri)-1);
    ts = tinv([0.025 0.975], numel(ri)-1);
    hmm_ri_stat(2:3,ik) = mean(ri) + ts*sem;
    
    ri = out.km_ari{ik};
    km_ri_stat(1,ik) = mean(ri);
    sem = std(ri) / sqrt(numel(ri)-1);
    ts = tinv([0.025 0.975], numel(ri)-1);
    km_ri_stat(2:3,ik) = mean(ri) + ts*sem;
end

% plot confidence interval
f1 = figure;

ax = gca;
ax.FontSize = 20;
ax.LineWidth = 1.3;
ax.YLim = [0 1];

hold on

p1 = plot(K, hmm_ri_stat(1,:), 'lineWidth', 4, 'Color', blue, ...
    'HandleVisibility', 'off', 'Marker', 's', ...
    'MarkerFaceColor', blue, 'MarkerSize', 10);
p2 = plot(K, km_ri_stat(1,:), 'lineWidth', 4, 'Color', red, ...
    'HandleVisibility', 'off', 'Marker', 's', ...
    'MarkerFaceColor', red, 'MarkerSize', 10);

fill([K fliplr(K)], [hmm_ri_stat(2,:) fliplr(hmm_ri_stat(3,:))], ...
    1, 'edgecolor', 'none', 'facealpha', 0.2, 'facecolor', blue);
fill([K fliplr(K)], [km_ri_stat(2,:) fliplr(km_ri_stat(3,:))], ...
    1, 'edgecolor', 'none', 'facealpha', 0.2, 'facecolor', red);

xticks(K);
xlabel('Number of Communities');
ylabel('Adjusted Rand Index');

box on
hold off

legend([p1 p2], {'MSS-SBM','K-means'},'Location','northwest');

outerpos = ax.OuterPosition;
ti = ax.TightInset; 
left = outerpos(1) + ti(1);
bottom = outerpos(2) + ti(2);
ax_width = outerpos(3) - ti(1) - ti(3);
ax_height = outerpos(4) - ti(2) - ti(4);
ax.Position = [left bottom ax_width ax_height];

clear sem ts km_ri_stat hmm_ri_stat ri

pos = f1.Position;

%%
print(f1, 'SimStateARI.eps','-depsc','-r300');
close all

%% 3 Averaged MSE
hmm_mse_stat = zeros(3,numel(K)); % mean, lower, up
km_mse_stat = zeros(3,numel(K)); % mean, lower, up

for ik = 1:numel(K)
    hmm_aver = mean(out.hmm_mse{ik},2,'omitnan');
    ikeep = ~isnan(hmm_aver)&~isinf(hmm_aver);
    hmm_aver = hmm_aver(ikeep);
    hmm_mse_stat(1,ik) = mean(hmm_aver,'omitnan');
    sem = std(hmm_aver) / sqrt(numel(hmm_aver)-1);
    ts = tinv([0.025 0.975], numel(hmm_aver)-1);
    hmm_mse_stat(2:3,ik) = mean(hmm_aver) + ts*sem;
    
    km_aver = mean(out.km_mse{ik},2,'omitnan');
    km_aver = km_aver(ikeep);
    km_mse_stat(1,ik) = mean(km_aver);
    sem = std(km_aver) / sqrt(numel(km_aver)-1);
    ts = tinv([0.025 0.975], numel(km_aver)-1);
    km_mse_stat(2:3,ik) = mean(km_aver) + ts*sem;
end

% plot confidence interval
f2 = figure;
ax = gca;
ax.FontSize = 20;
ax.LineWidth = 1.3;

hold on

fill([K fliplr(K)], [hmm_mse_stat(2,:) fliplr(hmm_mse_stat(3,:))], ...
    1, 'edgecolor', 'none', 'facealpha', 0.2, 'facecolor', blue);
fill([K fliplr(K)], [km_mse_stat(2,:) fliplr(km_mse_stat(3,:))], ...
    1, 'edgecolor', 'none', 'facealpha', 0.2, 'facecolor', red);

p1 = plot(K, hmm_mse_stat(1,:), 'lineWidth', 4, 'Color', blue, ...
    'HandleVisibility', 'off', 'Marker', 's', ...
    'MarkerFaceColor', blue, 'MarkerSize', 10);
p2 = plot(K, km_mse_stat(1,:), 'lineWidth', 4, 'Color', red, ...
    'HandleVisibility', 'off', 'Marker', 's', ...
    'MarkerFaceColor', red, 'MarkerSize', 10);

box on
hold off

legend([p1 p2], {'MSS-SBM','K-means'},'Location','northwest');
xticks(K);
xlabel('Number of Communities');
ylabel('Mean Squared Error');

outerpos = ax.OuterPosition;
ti = ax.TightInset; 
left = outerpos(1) + ti(1);
bottom = outerpos(2) + ti(2);
ax_width = outerpos(3) - ti(1) - ti(3);
ax_height = outerpos(4) - ti(2) - ti(4);
ax.Position = [left bottom ax_width ax_height];

f2.Position = pos;


%%
print(f2, 'SimStateMSE.eps','-depsc','-r300');
close all
%% 4 state series comparison: general
for ik = 1:numel(K)
    figure;
    subplot(3,1,1);
    stairs(real_states, 'LineWidth', 5);
    title('Real State Sequence');

    hmm_states_tmp = reshape(out.hmm_states{ik},T,[]);
    hmm_states_tmp = squeeze(hmm_states_tmp)';
    for i = 1:size(hmm_states_tmp,1)
        hmm_states_tmp(i,:) = ...
            permuteClasses(real_states,hmm_states_tmp(i,:));
    end
    subplot(3,1,2);
    imagesc(hmm_states_tmp);
    title('HMM Estimation');
    
    subplot(3,1,3);
    km_states_tmp = reshape(out.km_states{ik},T,[]);
    km_states_tmp = squeeze(km_states_tmp)';
    for i = 1:size(km_states_tmp,1)
        km_states_tmp(i,:) = ...
            permuteClasses(real_states,km_states_tmp(i,:));
    end
    nsub = size(km_states_tmp,1);
    imagesc(km_states_tmp);
    title('K-Means Estimation');

    suptitle(['Estimated State Sequence with ' ...
        num2str(K(ik)) ' communities']);
    print(['simulation_states' num2str(S) '_srep' num2str(Srep) ...
        '_inseq' num2str(inseq) '_compare_' num2str(K(ik)) '.png'],...
        '-r300','-dpng');
    close all
end
%% 5 state series comparison: MSE, tp / fp
hmm_stats = zeros(numel(K), S, 2, 2);
km_stats = zeros(numel(K), S, 2, 2);

% dim 3: mean, std
% dim 4: true positive, false positive, total assignments

for ik = 1:numel(K)
    for s = 1:S 
        true = real_states' == s;
        
        mse = squeeze(out.hmm_mse{ik}(:,s));
        pos = out.hmm_states{ik} == s;
        tp_hmm = mean(mean(pos(true==1,:,:),3),1);
        stds = [std(mse) std(tp_hmm)];
        means = [mean(mse) mean(tp_hmm)];
        hmm_stats(ik,s,:,:) = [means; stds];
        
        mse = squeeze(out.km_mse{ik}(:,s));
        pos = out.km_states{ik} == s;
        tp_km = mean(mean(pos(true==1,:,:),3),1);
        stds = [std(mse) std(tp_km)];
        means = [mean(mse) mean(tp_km)];
        km_stats(ik,s,:,:) = [means; stds];
    end
end


figure;
clr = {blue, red, yellow};

pars = ["MSE", "True Positives"];
for p = 1:2
    subplot(2,2,p);
    lgd = [];
    hold on
    for s = 1:S
        errorbar(K,hmm_stats(:,s,1,p),hmm_stats(:,s,2,p), ...
            'LineWidth',2, 'Color', clr{s});
    
        lgd = [lgd "$\hat{\Theta}_"+num2str(s)+"$: $\lambda$="+num2str(lambda(s))];
    end
    hold off
    grid;
    xticks(K);
    title(pars(p), 'fontsize', 12);
    if p == 1
        ylabel("HMM");
        curylim = get(gca, 'ylim');
        legend(lgd,'Interpreter','latex','Location','northwest','fontsize',6);
    end

    subplot(2,2,p+2);
    lgd = [];
    hold on
    for s = 1:S
        errorbar(K,km_stats(:,s,1,p),km_stats(:,s,2,p), ...
            'LineWidth',2, 'Color', clr{s});
    
        lgd = [lgd "$\hat{\Theta}_"+num2str(s)+"$: $\lambda$="+num2str(lambda(s))];
    end
    hold off
    grid;
    xticks(K);
    title(pars(p), 'fontsize', 12);
    if p == 1
        ylabel("K-Means");
        set(gca, 'YLim', curylim);
        legend(lgd,'Interpreter','latex','Location','northwest','fontsize',6);
    end
end

suptitle("Number of States = "+num2str(S));

%%
print(['hmm_km_mse_tp_state' num2str(S) '_srep' num2str(Srep) ...
    '_inseq' num2str(inseq) '_new.png'], '-dpng');
close all

%% true positive
hmm_stats = zeros(numel(K), 3);
km_stats = zeros(numel(K), 3);

% dim 3: mean, std
% dim 4: true positive, false positive, total assignments

for ik = 1:numel(K)
    pos_hmm = zeros(R,S);
    pos_km = zeros(R,S);
    for s = 1:S 
        true = real_states' == s;
        tmp = out.hmm_states{ik} == s;
        pos_hmm(:,s) = squeeze(mean(mean(tmp(true==1,:,:),3),1));
        
        tmp = out.km_states{ik} == s;
        pos_km(:,s) = squeeze(mean(mean(tmp(true==1,:,:),3),1));
    end
    
    pos_hmm = mean(pos_hmm,2);
    sem = std(pos_hmm) / sqrt(numel(pos_hmm)-1);
    ts = tinv([0.025 0.975], numel(pos_hmm)-1);
    hmm_stats(ik,1) = mean(pos_hmm);
    hmm_stats(ik,2:3) = mean(pos_hmm) + ts*sem;
    
    pos_km = mean(pos_km,2);
    sem = std(pos_km) / sqrt(numel(pos_km)-1);
    ts = tinv([0.025 0.975], numel(pos_km)-1);
    km_stats(ik,1) = mean(pos_km);
    km_stats(ik,2:3) = mean(pos_km) + ts*sem;
    
end


figure;
hold on

fill([K fliplr(K)], [hmm_stats(:,2)' fliplr(hmm_stats(:,3))'], ...
    1, 'edgecolor', 'none', 'facealpha', 0.2, 'facecolor', blue);
fill([K fliplr(K)], [km_stats(:,2)' fliplr(km_stats(:,3))'], ...
    1, 'edgecolor', 'none', 'facealpha', 0.2, 'facecolor', red);
legend('HMM','K-means','Location','northwest');

plot(K, hmm_stats(:,1), 'lineWidth', 1.5, 'Color', blue, ...
    'HandleVisibility', 'off', 'Marker', 'o', ...
    'MarkerFaceColor', blue, 'MarkerSize', 4);
plot(K, km_stats(:,1), 'lineWidth', 1.5, 'Color', red, ...
    'HandleVisibility', 'off', 'Marker', 'o', ...
    'MarkerFaceColor', red, 'MarkerSize', 4);

xticks(K);
xlabel('Number of Communities', 'fontsize', 12);
ylabel('True Positive');

grid
hold off

%%
print(['hmm_km_tp_state' num2str(S) '_srep' num2str(Srep) ...
    '_inseq' num2str(inseq) '_new.png'], '-dpng');
close all