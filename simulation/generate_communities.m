function [P,g] = generate_communities(N, K, lambda, alpha)
% function [P,g] = generate_communities(N, K, lambda, alpha)
%
% Generate community connectivity matrix using the stochastic block model.
%
%
% >>> INPUT
%
% - N: integer, number of nodes in the network
% - K: integer, number of communities
% - lambda: numeric, within-block connection probability
% - alpha: numeric, network sparsity
%
%
% >>> OUTPUT
%
% - g: N x 1 community membership vector
% - P: K x K community connectivity matrix
%
%
% >>> MISC
%
% Depends on `generateSbm`@`dyn_sbm` package
% Migrate from `test_multilayer_genlouvain` by Dr. Chee-ming Ting.

% Balanced community size (Equal parition of nodes)

if nargin < 4
    alpha = 1;
end

g = generate_membership(N,K);

% Synthetic density matrix
rng("shuffle");
a = -0.2; b = 0.2; 
pr = a + (b-a)*rand(1,1); % Subject-specific random deviation in block probability
pii = lambda + pr; % within-block connection probability
% pij = (1 - pii)/(K-1); % between-block connection probability
% P(:,:) = pii.*eye(K) + pij.*ones(K) - pij.*eye(K);
pij = 1 - pii;
P(:,:) = pii.*eye(K) + pij.*ones(K);
P = alpha.*P;

% within-block connectivity: alpha
% between-block connectivity: alpha*(1-lambda-pr)

end