%==========================================================================
%  Simulation - multilayer community detection
%
%  6-1-2020 Chee-Ming Ting
%  8-1-2020 Modified from sim_multilayerSBM: Fixed K and R while N increase
%==========================================================================
clc; clear all; close all;
cd('C:\Users\User\Desktop\RESEARCH\NetModule\multilayer_nets');
addpath('C:\Users\User\Desktop\RESEARCH\NetModule\dyn_sbm');
addpath('C:\Users\User\Desktop\RESEARCH\NetModule\GenLouvain-master');

N_range = 20:20:120; % number of nodes in network
K = 5; % number of communities
R = 100; % number of layers/subjects

nEst = 3; % Number of competitor methods
ARI.Spec = zeros(R,length(N_range)); % Spectral clustering
ARI.SM   = zeros(R,length(N_range)); % Single-layer modularity maximization
ARI.MM   = zeros(R,length(N_range)); % Multi-layer modularity maximization
ARI.mean = zeros(nEst,length(N_range));
ARI.std  = zeros(nEst,length(N_range));

% NMI.Spec = zeros(R,lengtth(N_range));
% NMI.std  = zeros(nEst,lengh(N_range)); % Spectral clustering
% NMI.SM   = zeros(R,length(N_range)); % Single-layer modularity maximization
% NMI.MM   = zeros(R,length(N_range)); % Multi-layer modularity maximization
% NMI.mean = zeros(nEst,length(N_range));

alpha = 0.8;
% alpha = 1;

%-------------------------------------------------------------------------%
%      Effect of increasing N on estimated community partition            %
%-------------------------------------------------------------------------%
for Ni = 1:length(N_range)
    
N = N_range(Ni);
    
g = zeros(N,1); % community membership vector
P = zeros(K,K); % edge probability/ density matrix
W = zeros(N,N,R); % multilayer binary graphs

ghatSpec = zeros(N,R);
ghatSM = zeros(N,R);
ghatMM = zeros(N,R);

% Balanced community size (Equal parition of nodes)
n_k = N/K; % number of nodes per community
for k=1:K
    g(n_k*(k-1)+1:n_k*(k-1)+n_k,1) = k;
end

%-------------------------------------------------------------------------%
%                     Simulate multilayer graph                           %
%-------------------------------------------------------------------------%
for r=1:R
    
    % Synthetic density matrix
%     a = -0.1; b = 0.1;
    a = -0.2; b = 0.2; 
    pr = a + (b-a)*rand(1,1); % Subject-specific random deviation in block probability
    pii = 0.7 + pr; % within-block connection probability
    pij = (1 - pii); % between-block connection probability
    P(:,:) = pii*eye(K) + pij*ones(K);
    P = alpha*P;
    
%     pij = (1 - pii)/(K-1); % between-block connection probability
%     P(:,:) = pii*eye(K) + pij*ones(K) - pij*eye(K);
    
    % Generate subject-specific adjacency matrix
    W(:,:,r) = generateSbm(g,P);
%     figure(r); imagesc(W(:,:,r)); colormap([1 1 1; 0 0 0]);
end

%-------------------------------------------------------------------------%
%                    Single-layer community detection                     %
%-------------------------------------------------------------------------%

% Single-subject spectral clustering
Opt = {};
Opt.nKmeansReps = 10;
for r=1:R
%     ghatSpec(:,r) = spectralcluster(W(:,:,r),K);
    [ghatSpec(:,r),~,~,~] = spectralClusterSbm(W(:,:,r),K,Opt);
end

% Single-layer modularity maximization
for r=1:R
    [ghatSM(:,r),~] = community_louvain(W(:,:,r));
%     [ghatSM(:,r),~] = modularity_und(W(:,:,r));
end

%-------------------------------------------------------------------------%
%                     Multilayer community detection                      %
%-------------------------------------------------------------------------%
Wall = cell(1,R);
for r=1:R
    Wall{r} = W(:,:,r);
end

% Multilayer modularity maximization (Categorical Multislice Network)
omega = 1;
gamma = 1;
N=length(Wall{1});
R=length(Wall);
B=spalloc(N*R,N*R,(N+R)*N*R);
twomu=0;
for r=1:R
    k=sum(Wall{r});
    twom=sum(k);
    twomu=twomu+twom;
    indx=[1:N]+(r-1)*N;
    B(indx,indx)=Wall{r}-gamma*k'*k/twom;
end
twomu=twomu+R*omega*N*(R-1);
all2all = N*[(-R+1):-1,1:(R-1)];
B = B + omega*spdiags(ones(N*R,2*R-2),all2all,N*R,N*R);
[S,Q] = genlouvain(B);
Q = Q/twomu;
S = reshape(S,N,R);
ghatMM(:,:) = multislice_pair_labeling(S);


%-------------------------------------------------------------------------%
%                   Performance Evaluation - Adjusted RI                  %  
%-------------------------------------------------------------------------%
for r=1:R
    [ARI.Spec(r,Ni),~,~,~] = RandIndex(g,ghatSpec(:,r));
    [ARI.SM(r,Ni),~,~,~]   = RandIndex(g,ghatSM(:,r));
    [ARI.MM(r,Ni),~,~,~]   = RandIndex(g,ghatMM(:,r));
%     NMI.Spec(r,Ni) = getNMI(g,ghatSpec(:,r));
%     NMI.SM(r,Ni)   = getNMI(g,ghatSM(:,r));
%     NMI.MM(r,Ni)   = getNMI(g,ghatMM(:,r));
end

ARI.mean(1,Ni) = mean(ARI.Spec(:,Ni));
ARI.mean(2,Ni) = mean(ARI.SM(:,Ni));
ARI.mean(3,Ni) = mean(ARI.MM(:,Ni));
ARI.std(1,Ni) = std(ARI.Spec(:,Ni));
ARI.std(2,Ni) = std(ARI.SM(:,Ni));
ARI.std(3,Ni) = std(ARI.MM(:,Ni));

% NMI.mean(1,Ni) = mean(NMI.Spec(:,Ni));
% NMI.mean(2,Ni) = mean(NMI.SM(:,Ni));
% NMI.mean(3,Ni) = mean(NMI.MM(:,Ni));
% NMI.std(1,Ni) = std(NMI.Spec(:,Ni));
% NMI.std(2,Ni) = std(NMI.SM(:,Ni));
% NMI.std(3,Ni) = std(NMI.MM(:,Ni));

end

%-------------------------------------------------------------------------%
%                           Ploting Results                               %
%-------------------------------------------------------------------------%
fig_dir  = 'FIGURES\';
out_dir  = 'OUTPUT\';

FigName = strcat('RandInd-effN');
figure('Name',FigName,'Color',[0.95 0.95 0.95]);
set(gcf, 'PaperPositionMode', 'manual'); set(gcf, 'PaperUnits', 'inches');
set(gcf, 'PaperPosition', [0 0 5 4]);
errorbar(N_range,ARI.mean(1,:),ARI.std(1,:),'-o','Color',[255,128,0]/255,'LineWidth',2.3,'MarkerSize',8); hold on;
errorbar(N_range,ARI.mean(2,:),ARI.std(2,:),'-d','Color',[65,105,225]/255,'LineWidth',2.3,'MarkerSize',8);  hold on;
errorbar(N_range,ARI.mean(3,:),ARI.std(3,:),'-s','Color',[0 51 102]/255,'LineWidth',2.3,'MarkerSize',8); hold on;
xlim([N_range(1)-0.2 N_range(end)+0.2]);
set(gca,'XTick',N_range,'fontsize',12);
legend({'Spectral clustering';'Single-layer MM';'Multilayer MM'}, 'location', 'southeast', 'fontsize',10);
xlabel('Number of Nodes', 'fontsize',12);
ylabel('Adjusted Rand Index', 'fontsize',12);
svFigName=strcat(fig_dir,FigName,'.eps'); saveas(gcf,svFigName,'epsc2');

% FigName = strcat('NMI-effN');
% figure('Name',FigName,'Color',[0.95 0.95 0.95]);
% set(gcf, 'PaperPositionMode', 'manual'); set(gcf, 'PaperUnits', 'inches');
% set(gcf, 'PaperPosition', [0 0 5 4]);
% errorbar(N_range,NMI.mean(1,:),NMI.std(1,:),'-o','Color',[255,128,0]/255,'LineWidth',2.3,'MarkerSize',8); hold on;
% errorbar(N_range,NMI.mean(2,:),NMI.std(2,:),'-d','Color',[65,105,225]/255,'LineWidth',2.3,'MarkerSize',8);  hold on;
% errorbar(N_range,NMI.mean(3,:),NMI.std(3,:),'-s','Color',[0 51 102]/255,'LineWidth',2.3,'MarkerSize',8); hold on;
% xlim([N_range(1)-0.2 N_range(end)+0.2]);
% set(gca,'XTick',N_range,'fontsize',12);
% legend({'Spectral clustering';'Single-layer MM';'Multilayer MM'}, 'location', 'southeast', 'fontsize',10);
% xlabel('Number of Nodes', 'fontsize',12);
% ylabel('Normalized Mutual Informatio', 'fontsize',12);
% svFigName=strcat(fig_dir,FigName,'.eps'); saveas(gcf,svFigName,'epsc2');

filename=strcat(out_dir,'sim_multisbm_effN','-K',num2str(K),'-R',num2str(R),'.mat');
% save(filename,'K','R','N_range','pii','omega','gamma','ARI','NMI','-mat');
save(filename,'K','R','N_range','pii','omega','gamma','ARI','-mat');

