%==========================================================================
%  Estimating dynamic network communities in movie fMRI
%
%  31-10-2017 Chee-Ming Ting
%==========================================================================
clc; clear all; close all;
cd('C:\Users\0wner\Desktop\Movie-fMRI\dyn_sbm');

%-------------------------------------------------------------------------%
%                       Single-subject Analysis                           %
%-------------------------------------------------------------------------%
input_dir = 'C:\Users\0wner\Desktop\Movie-fMRI\Data\FS_16ROI_mean\';
% sub_label = {'6251_mean_fs_heavy','6389_mean_fs_heavy','6492_mean_fs_heavy','6550_mean_fs_heavy','6551_mean_fs_heavy','6781_mean_fs_heavy','6791_mean_fs_heavy','6617_mean_fs_heavy','6643_mean_fs_heavy','6648_mean_fs_heavy','6737_mean_fs_heavy','6743_mean_fs_heavy','6756_mean_fs_heavy','6769_mean_fs_heavy'};
sub_label = {'6251_mean_fs_heavy'};

% load data    
sub_file_name = sub_label{1};
filename=strcat(input_dir,sub_file_name,'.mat');
load(filename);
y = mean_roi; 
[N,T] = size(y);

% Demean & Normalization to -1 and 1
for i = 1:N
    y(i,:) = y(i,:) - mean(y(i,:));
%      y(i,:) = (y(i,:)-mean(y(i,:)))/std(y(i,:));
%      y(i,:) = medfilt1(y(i,:),2);
%     y(i,:) = (y(i,:)-min(y(i,:)))/(max(y(i,:))-min(y(i,:)))*2 - 1; 
end

%-------------------------------------------------------------------------%
%            Time-varying adjacency matrices by sliding-window            %
%-------------------------------------------------------------------------%
C = zeros(N,N);
adj = zeros(N,N,T);

wlen = 50; % Window length
shift = 1; % window shift
win = rectwin(wlen); % form a window

% Initialize indexes
indx = 0; t = 1;
Yw   = zeros(N,wlen);
yx = [y y(:,(T-wlen)+1:T)];

% Sliding window Analysis
lambda = 0.1;
while indx < T
    % while indx + wlen <= T
    for i=1:N
        Yw(i,:) = yx(i,indx+1:indx+wlen).*win'; end
    C = partialcorr(Yw');
    C(abs(C)<lambda) = 0; C(abs(C)>lambda) = 1;
    C(1:N+1:end) = 0; % Set diagonals zero
    adj(:,:,t) = C;
    indx = indx + shift; t = t + 1;
end

%-------------------------------------------------------------------------%
%     Estimating time-evolving network communities with dynamic SBM       %
%-------------------------------------------------------------------------%
% Parameters for unknown classes
kPost = 2;
pPost = kPost^2;
initCovPost = eye(pPost);
stateCovInPost = 0.1;
stateCovOutPost = 0.02;

% Optional parameters
Opt.directed = true;
% Number of random initializations for k-means step of spectral clustering
Opt.nKmeansReps = 5;
Opt.maxIter = 200;  % Maximum number of local search iterations
Opt.output = 0; % Level of output to display in console

% Estimate states using EKF with classes estimated by local search
stateTransPost = eye(pPost);
stateCovPost = generateStateCov(kPost,stateCovInPost,stateCovOutPost,true);

OptPostEkf = Opt;
OptPostEkf.nClasses = kPost;
[classPostEkf,psiPostEkf,psiCovPostEkf,~,OptPostEkf] = ekfDsbmLocalSearch ...
    (adj,kPost,stateTransPost,stateCovPost,[],[],initCovPost,OptPostEkf);

logistic = @(x) 1./(1+exp(-x));	% Logistic function
thetaPostEkf = logistic(psiPostEkf);

% Plot time-varying class membership of nodes
FigName=strcat('TV-class','-sub',sub_file_name);
figure('Name',FigName,'Color',[1 1 1]);
cmap = [0 25 102;0,102,204;102,205,170;255,140,0;255,230,0]/255;
imagesc(classPostEkf); colormap(cmap);
xlim([1 T]); set(gca,'XTick',100:100:T,'fontsize',11);
Channel_labelsY = {'CG.L','PP.L','PT.L','PoCG.L','PreCG.L','STG.L','TTG.L','TTS.L','CG.R','PP.R','PT.R','PoCG.R','PreCG.R','STG.R','TTG.R','TTS.R'};
set(gca,'YTick',1:1:N,'YTickLabel',Channel_labelsY);
xlabel('Time Point', 'fontsize',12);
ylabel('ROI', 'fontsize',12);

%-------------------------------------------------------------------------%
%          Identifying State-related changes in community structure       %
%-------------------------------------------------------------------------%
K = 2; % Temporal clusters
opts = statset('Display','final');
[St_sbm,~,sumD,d] = kmeans(thetaPostEkf',K,'Distance','sqeuclidean','Replicates',50,'start','cluster','Options',opts);

t=1:1:T;
FigName = strcat('Dyn-module-states-','-sub',sub_file_name);
figure('Name',FigName,'Color',[1 1 1]);
plot(t,St_sbm,'Color',[0 102 204]/255,'LineWidth',2); hold on;
xlim([1 T]); ylim([.75 K+.25]);
set(gca,'XTick',100:100:T,'fontsize',11); set(gca,'YTick',1:1:K,'fontsize',11);
xlabel('Time Point', 'fontsize',12);
ylabel('States', 'fontsize',12);


%-------------------------------------------------------------------------%
%               Estimate State-specific adjacency matrix                  %
%-------------------------------------------------------------------------%
% Pooling samples for regimes
St = zeros(N,T,K);
tj = zeros(K,1);
for j=1:K
    t=1;
    for i=1:T
        if St_sbm(i)==j,
           St(:,t,j)=y(:,i);  t=t+1;
        end
    end
    tj(j) = t-1;
end

% Estiamte State-specific VAR
adj_j = zeros(N,N,K);
for j=1:K
    C = partialcorr(St(:,1:tj(j),j)');
    C(abs(C)<lambda) = 0; C(abs(C)>lambda) = 1;
    C(1:N+1:end) = 0; % Set diagonals zero
    adj_j(:,:,j) = C;
    webweb(C);
end


