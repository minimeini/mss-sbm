%==========================================================================
%  Estimate Community structure at each state
%==========================================================================
clc; clear all; close all;
cd('C:\Users\0wner\Desktop\Movie-fMRI\dyn_sbm');

%-------------------------------------------------------------------------%
%                     Load Connectivity matrix                            %
%-------------------------------------------------------------------------%
out_dir  = 'C:\Users\0wner\Desktop\Movie-fMRI\Est_output\';

% typeFC = 'FS-PCOH-grp-';
% filename=strcat(out_dir,'FS-PCOH-grp-p1Kg2K5-heavy','.mat');

typeFC = 'FS-PCOH-grp-';
filename=strcat(out_dir,'FS-PCOH-grp-p1Kg3K5-heavy','.mat');

% typeFC = 'FS-PCOH-grp-';
% filename=strcat(out_dir,'FS-PCOH-grp-p1Kg5K5-heavy','.mat');
load(filename);
nROI = N;

C = zeros(N,N);
FC_adj = zeros(N,N,Kgrp);
% lambda = 0.4;
lambda = 0.5;
for j=1:Kgrp
    C(:,:) = FC_grp(:,:,j);
    C(abs(C)<lambda) = 0; C(abs(C)>lambda) = 1;
    C(1:N+1:end) = 0; % Set diagonals zero
    FC_adj(:,:,j) = C;
end


%-------------------------------------------------------------------------%
%                     Estimate communitiy structure                       %
%-------------------------------------------------------------------------%
% k = 2;  % Number of communities
% directed = false;   % Whether snapshots are directed or undirected
% % Number of states
% if directed == true
%     p = k^2;
% else
%     p = k*(k+1)/2;
% end
% 
% Opt.directed = false;
% % Number of random initializations for k-means step of spectral clustering
% Opt.nKmeansReps = 20;
% Opt.svdType = 'full'; 
% Opt.embedDim = k;
% 
% % Estimate states using static spectral clustering at each time step
% classPostStatic = zeros(N,Kgrp);
% classCentroid = zeros(k,k,Kgrp);
% thetaPostStatic = zeros(p,Kgrp);
%  for j=1:Kgrp
%      [classPostStatic(:,j),~,~,OptPostStatic] = spectralClusterSbm(FC_adj(:,:,j),k,Opt);
% %      [classPostStatic(:,j),classCentroid(:,:,j),~,~,OptPostStatic] = spectralClusterSbm2(FC_adj(:,:,j),k,Opt);
% %      [thetaPostStatic(:,j),~,OptPostStatic] = estimateSbmProb(FC_adj(:,:,j),classPostStatic(:,j),OptPostStatic);
%  end
% 
%  % Compute community structure matrix
% W = zeros(N,N,Kgrp);
% for j=1:Kgrp
%     for n=1:N
%         for m=1:N
%             if(classPostStatic(n,j) == classPostStatic(m,j))
%                 W(n,m,j) = 1;
%             end
%         end
%     end
% end

%-------------------------------------------------------------------------%
%                     Plot community graph                                %
%-------------------------------------------------------------------------%
Channel_labelsY = {'CG.L','PP.L','PT.L','PoCG.L','PreCG.L','STG.L','TTG.L','TTS.L','CG.R','PP.R','PT.R','PoCG.R','PreCG.R','STG.R','TTG.R','TTS.R'};
Channel_labelsX = {'CG.L','PP.L','PT.L','PoCG.L','PreCG.L','STG.L','TTG.L','TTS.L','CG.R','PP.R','PT.R','PoCG.R','PreCG.R','STG.R','TTG.R','TTS.R'};

rChannel_labelsY = cell(1,N);
rChannel_labelsX = cell(1,N);

Cj = zeros(N,Kgrp);
Q = zeros(1,Kgrp);

for j=1:Kgrp
    
    % Binary matrix
    FC_adj_j = squeeze(FC_adj(:,:,j));
%     [Cj(:,j),Q] = modularity_und(FC_adj(:,:,j));
    [Cj(:,j),Q] = community_louvain(FC_adj(:,:,j));
    [X,Y,INDSORT] = grid_communities(Cj(:,j));
     FigName = strcat(typeFC,'lv-bin','p',num2str(p),'Kg',num2str(Kgrp),'K',num2str(K),'-state',num2str(j),'-grp');
    figure('Name',FigName,'Color',[1 1 1]);
    colormap([1 1 1; 0 0 0]);
    imagesc(FC_adj_j(INDSORT,INDSORT)); hold on;
    plot(X,Y,'r','linewidth',3);
    for i=1:N
        rChannel_labelsY(1,i) = Channel_labelsY(1,INDSORT(i));
        rChannel_labelsX(1,i) = Channel_labelsX(1,INDSORT(i));
    end
    ylabel('ROIs','FontSize',14,'fontweight','bold'); xlabel('ROIs','FontSize',14,'fontweight','bold');
    set(gca,'XTick',1:1:nROI,'XTickLabel',rChannel_labelsX);
    set(gca,'YTick',1:1:nROI,'YTickLabel',rChannel_labelsY); set(gca,'FontSize',14);
    xticklabel_rotate;
    
    % Weighted undirected matrix
    FC_grp_j = squeeze(FC_grp(:,:,j));
%     [Cj(:,j),Q(1,j)] = modularity_und(FC_grp(:,:,j));
    [Cj(:,j),Q(1,j)] = community_louvain(FC_grp(:,:,j),1.055);
    [X,Y,INDSORT] = grid_communities(Cj(:,j));
    FigName = strcat(typeFC,'lv-weight','p',num2str(p),'Kg',num2str(Kgrp),'K',num2str(K),'-state',num2str(j),'-grp');
    figure('Name',FigName,'Color',[1 1 1]);
%     colormap(jet);
    imagesc(FC_grp_j(INDSORT,INDSORT)); hold on;
    plot(X,Y,'k','linewidth',3);
    
    for i=1:N
        rChannel_labelsY(1,i) = Channel_labelsY(1,INDSORT(i));
        rChannel_labelsX(1,i) = Channel_labelsX(1,INDSORT(i));
    end
    ylabel('ROIs','FontSize',14,'fontweight','bold'); xlabel('ROIs','FontSize',14,'fontweight','bold');
    set(gca,'XTick',1:1:nROI,'XTickLabel',rChannel_labelsX);
    set(gca,'YTick',1:1:nROI,'YTickLabel',rChannel_labelsY); set(gca,'FontSize',14);
    xticklabel_rotate;

end

%-------------------------------------------------------------------------%
%                  Plot group-level states                                %
%-------------------------------------------------------------------------%
% out_dir_fig  = 'C:\Users\0wner\Desktop\Movie-fMRI\LATEX\FIGURES\';
% Channel_labelsY = {'CG.L','PP.L','PT.L','PoCG.L','PreCG.L','STG.L','TTG.L','TTS.L','CG.R','PP.R','PT.R','PoCG.R','PreCG.R','STG.R','TTG.R','TTS.R'};
% Channel_labelsX = {'CG.L','PP.L','PT.L','PoCG.L','PreCG.L','STG.L','TTG.L','TTS.L','CG.R','PP.R','PT.R','PoCG.R','PreCG.R','STG.R','TTG.R','TTS.R'};
% 
% % Plot state-specific adjacency matrix 
% for j=1:Kgrp
%     FigName = strcat(typeFC,'state-FC-adj','p',num2str(p),'Kg',num2str(Kgrp),'K',num2str(K),'-state',num2str(j),'-grp');
%     figure('Name',FigName,'Color',[1 1 1]);
% %     imagesc(squeeze(FC_adj(:,:,j)));
%     imagesc(squeeze(W(:,:,j))); 
%     colormap([1 1 1; 0 0 0]);
% %     h=colorbar;  axis square;
% %     caxis([min(FC_grp(:)) max(FC_grp(:))]);
%     ylabel('ROIs','FontSize',18,'fontweight','bold'); xlabel('ROIs','FontSize',18,'fontweight','bold');
%     set(gca,'XTick',1:1:nROI,'XTickLabel',Channel_labelsX); set(gca,'YTick',1:1:nROI,'YTickLabel',Channel_labelsY);
%     xticklabel_rotate;
%     set(gca,'FontSize',16);
% %     svFigName=strcat(out_dir_fig,FigName,'.eps'); saveas(gcf,svFigName,'epsc2');
% end
