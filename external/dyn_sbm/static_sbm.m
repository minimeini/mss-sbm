%==========================================================================
%  Estimating dynamic network communities in movie fMRI
%
%  31-10-2017 Chee-Ming Ting
%==========================================================================
clc; clear all; close all;
cd('C:\Users\0wner\Desktop\Movie-fMRI\dyn_sbm');

%-------------------------------------------------------------------------%
%                       Single-subject Analysis                           %
%-------------------------------------------------------------------------%
input_dir = 'C:\Users\0wner\Desktop\Movie-fMRI\Data\FS_16ROI_mean\';
% sub_label = {'6251_mean_fs_heavy','6389_mean_fs_heavy','6492_mean_fs_heavy','6550_mean_fs_heavy','6551_mean_fs_heavy','6781_mean_fs_heavy','6791_mean_fs_heavy','6617_mean_fs_heavy','6643_mean_fs_heavy','6648_mean_fs_heavy','6737_mean_fs_heavy','6743_mean_fs_heavy','6756_mean_fs_heavy','6769_mean_fs_heavy'};
sub_label = {'6251_mean_fs_heavy'};

% load data    
sub_file_name = sub_label{1};
filename=strcat(input_dir,sub_file_name,'.mat');
load(filename);
y = mean_roi; 
[N,T] = size(y);

% Demean & Normalization to -1 and 1
for i = 1:N
    y(i,:) = y(i,:) - mean(y(i,:));
%      y(i,:) = (y(i,:)-mean(y(i,:)))/std(y(i,:));
%      y(i,:) = medfilt1(y(i,:),2);
%     y(i,:) = (y(i,:)-min(y(i,:)))/(max(y(i,:))-min(y(i,:)))*2 - 1; 
end

%-------------------------------------------------------------------------%
%            Time-varying adjacency matrices by sliding-window            %
%-------------------------------------------------------------------------%
C = zeros(N,N);
adj = zeros(N,N,T);

wlen = 200; % Window length
shift = 1; % window shift
win = rectwin(wlen); % form a window

% Initialize indexes
indx = 0; t = 1;
Yw   = zeros(N,wlen);
yx = [y y(:,(T-wlen)+1:T)];

% Sliding window Analysis
lambda = 0.1;
while indx < T
    % while indx + wlen <= T
    for i=1:N
        Yw(i,:) = yx(i,indx+1:indx+wlen).*win'; end
    C = partialcorr(Yw');
    C(abs(C)<lambda) = 0; C(abs(C)>lambda) = 1;
    C(1:N+1:end) = 0; % Set diagonals zero
    adj(:,:,t) = C;
    indx = indx + shift; t = t + 1;
end

%-------------------------------------------------------------------------%
%         Estimating time-evolving network communities with SBM           %
%-------------------------------------------------------------------------%
k = 3;  % Number of communities
directed = false;   % Whether snapshots are directed or undirected
% Number of states
if directed == true
    p = k^2;
else
    p = k*(k+1)/2;
end

Opt.directed = false;
% Number of random initializations for k-means step of spectral clustering
Opt.nKmeansReps = 5;
Opt.svdType = 'full'; 
Opt.embedDim = k;

% Estimate states using static spectral clustering at each time step
classPostStatic = zeros(N,T);
classCentroid = zeros(k,k,T);
thetaPostStatic = zeros(p,T);
 for t = 1:T
%      [classPostStatic(:,t),~,~,OptPostStatic] = spectralClusterSbm(adj(:,:,t),k,Opt);
     [classPostStatic(:,t),classCentroid(:,:,t),~,~,OptPostStatic] = spectralClusterSbm2(adj(:,:,t),k,Opt);
%      [thetaPostStatic(:,t),~,OptPostStatic] = estimateSbmProb(adj(:,:,t),classPostStatic(:,t),OptPostStatic);
 end
 
 Ut = zeros(N,k,T);
 Utvec = zeros(N*k,T);
 for t = 1:T
     for i = 1:N
         Ut(i,:,t) = classCentroid(classPostStatic(i,t),:,t);
     end
     Utemp = squeeze(Ut(:,:,t));
     Utvec(:,t) = Utemp(:);
 end
 
K = 2; % Temporal clusters
opts = statset('Display','final');
[St,~,sumD,d] = kmeans(Utvec',K,'Distance','cosine','Replicates',50,'start','cluster','Options',opts);

t=1:1:T;
FigName = strcat('Dyn-module-states-','p',num2str(p),'K',num2str(K),'-sub',sub_file_name);
figure('Name',FigName,'Color',[1 1 1]);
plot(t,St,'Color',[0 102 204]/255,'LineWidth',2); hold on;
xlim([1 T]); ylim([.75 K+.25]);
set(gca,'XTick',100:100:T,'fontsize',11); set(gca,'YTick',1:1:K,'fontsize',11);
xlabel('Time Point', 'fontsize',12);
ylabel('States', 'fontsize',12);

