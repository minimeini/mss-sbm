# Multi-subject, Markov-switching Stochastic Block Model (MSS-SBM)

A multi-subject, Markov-switching stochastic block model (MSS-SBM) to identify state-related changes in brain community organization over a group of individuals.

For more detailed description and applications, please refer to

Ting, C.-M., Samdin, S. B., Tang, M., and Ombao, H. (2020). Detecting Dynamic Community Structure in Functional Brain Networks Across Individuals: A Multilayer Approach. http://arxiv.org/abs/2004.04362


## Dependencies

- [Brain Connectivity Toolbox (BCT)](https://www.nitrc.org/projects/bct/)
- [Dynamic Stochastic Block Model](https://github.com/IdeasLabUT/Dynamic-Stochastic-Block-Model)
- [GenLouvain](http://netwiki.amath.unc.edu/GenLouvain)
- [HMM](https://www.cs.ubc.ca/~murphyk/Software/HMM/hmm.html)
- [boxplot2](https://github.com/kakearney/boxplot2-pkg)

The dependencies are included in the `external` folder.

## Example

- Simulation: `./example_simulation.m`





