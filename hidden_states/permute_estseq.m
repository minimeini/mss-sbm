function [EstSeq,BestPerm] = permute_estseq(TrueSeq,EstSeq)
cdir = fileparts(mfilename('fullpath'));
cdir = strsplit(cdir,'/');
cdir = strjoin(cdir(1:end-1),'/');
addpath(strcat(cdir,"/external/dyn_sbm"));
assert(isequal(unique(TrueSeq(:)),unique(EstSeq(:))),...
    'States in the estimated sequence are different from the true sequence');
nsub = size(EstSeq,2);
nstate = numel(unique(TrueSeq(:)));
BestPerm = zeros(nstate,nsub);
for s = 1:nsub
    [EstSeq(:,s),BestPerm(:,s)] = ...
        permuteClasses(TrueSeq(:,s),EstSeq(:,s));
end
end