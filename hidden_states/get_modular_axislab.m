function [AxisLab,CommTab,CommFlag,nCommAll,CommEstSort] = get_modular_axislab(CommEst,NodeName,ncomm)

nCommAll = numel(unique(CommEst(:)));
if isempty(ncomm)
    ncomm = nCommAll;
end

CommTab = tabulate(CommEst);
[~,idx] = sort(CommTab(:,3),'descend');
%
CommTab = CommTab(idx,:);
CommMax = CommTab(1:ncomm,1)';
NodeIdx = find(arrayfun(@(x) ismember(x,CommMax),CommEst));
CommFlag = arrayfun(@(x) ismember(x,CommMax),1:nCommAll);

CommTab = CommTab(1:ncomm,:);

for i = 1:ncomm
    tmp = NodeName(CommEst==CommTab(i,1));
    if size(tmp,1)>1, tmp = tmp'; end
    if i == 1
        AxisLab = tmp;
    else
        AxisLab = [AxisLab tmp];
    end
end

CommEstSort = [];
for i = 1:ncomm
    CommEstSort = [CommEstSort CommEst(CommEst==CommTab(i,1))'];
end

end