function [mu0,Sigma0,km_clusters] = ...
    initialize_hmm(nin,nmix,S,data,km_clusters,km_centroids,cov_type)
% >>> Input
% - data: nin x ntime
% - init_states: [] or ntime x S
%
% >>> Output
% - mu0: nin x nstate, mean
% - Sigma0: nin x nin x nstate, variance-covariance matrix


if isempty(km_clusters) || isempty(km_centroids) || nmix>1
    try
        [mu0, Sigma0, ~, km_clusters] = ...
            mixgauss_init(S*nmix, data, cov_type, 'kmeans');
    catch err
        if strcmp(err.identifier, 'MATLAB:svd:matrixWithNaNInf')
            warning(['mixgauss_init: loc=1 ' err.identifier]);
        end
        rethrow(err);
    end
else
    mu0 = zeros(nin,S);
    Sigma0 = zeros(nin,nin,S);
    for s = 1:S
        % Pick out data points belonging to this centre
        tmp = data(:,km_clusters(:,s)==1)'; % nobs x nin
        mu0(:,s) = km_centroids(s,:);
        diffs = tmp - (ones(size(tmp,1),1) * km_centroids(s,:));
        Sigma0(:,:,s) = (diffs'*diffs)/(size(tmp,1));
        % Add GMM_WIDTH*Identity to rank-deficient covariance matrices
        if rank(Sigma0(:,:,s)) < nin
            Sigma0(:,:,s) = Sigma0(:,:,s) + 1.0*eye(nin);
        end
    end
end

mu0 = reshape(mu0, [nin S nmix]);
Sigma0 = reshape(Sigma0, [nin nin S nmix]);

end