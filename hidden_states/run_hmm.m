function [HMMStates,KMStates,lmax,mse,hmm_flag,hmm_iter,...
    transmat,Mu,Sigma,runtime] = run_hmm(PostConnOddsCom, ...
    ThetasVec, S, transmat0, nmix, nwait, varargin)
% function [HMMStates,KMStates,lmax,mse,hmm_flag,hmm_iter,...
%     transmat,Mu,Sigma,runtime] = run_hmm(PostConnOddsCom, ...
%     PostConnProbCom, Thetas, S, transmat0, nmix, nwait)
%
% A wrapper of `class_hmm` tries to get a better result with higher
% log likelihood.
% 
% >>> Algorithm
%
% The idea is that if in the next `nwait` consecutive runs we cannot 
% find a outcome has a higher log likelihood, we assume this is a optimal 
% estimation. If we find a outcome has a higher log likelihood, then we
% need to count another `nwait` consecutive runs.
%
% 1. Run the `class_hmm` and get an valid estimation. Set rcnt = 1.
% 2. Rerun the `class_hmm` and see if the log likelihood in this run is
% higher than the one obtained in Step 1.
%    2.1 If the log likelihood is higher, save the estimation in step 2 and
%    discard the estimation we got in step 1. Set rcnt back to 1.
%    2.2 If the log likelihood is not higher, discard the estimation in
%    step 2. Set rcnt = rcnt + 1.
% 3. Repeat step 2 until rcnt == nwait.
%
%
% >>> Input
%
% - PostConnOddsCom: nin x ntime matrix, estimated individual ThetaReal
% - PostConnProbCom: nin x ntime matrix, estimated individual ThetaProb, 
% only used in simulation to calculate the MSE. Set it to [] for real data.
% - Thetas: nin x ntime matrix, true individual time-varying Theta in
% (0,1), only used in simulation to calculate the MSE. Set in to [] for
% real data.
% - S: integer, number of states
% - transmat0: S X S matrix, initial value of transition matrix.
% - nmix: number of Gaussian mixtures.
% - nwait: number of consecutive counts we need to `believe` the current
% estimation is an `optimum`.

%%
transmat = [];
Mu = [];
Sigma = [];
hmm_iter = 0;
hmm_flag = 0;
rcnt = 0;
lmax = -99999;
first_print = 1;
runtime = [];
mse = [];
HMMStates = [];
KMStates = [];

cov_type = 'full';
if nargin > 7
    cov_type = varargin{1};
end

verbose = true;
if nargin > 8
    verbose = varargin{2};
end

real_states = [];
if nargin > 9
    real_states = varargin{3};
end
   
%%
logistic = @(x) 1./(1+exp(-x)); %logistic function


%%
while (hmm_flag == 0) || (rcnt < nwait)

    tstart = tic;
    [HMMStatesTmp,EstPara,irun,KMStatesTmp,hmm_flag] = ...
        class_hmm(PostConnOddsCom,S,transmat0,nmix,cov_type);
    rtmp = toc(tstart);
    
    runtime = [runtime rtmp];
    hmm_iter = hmm_iter + sum(irun);
    
    %% 2.4 Calculate the mean square error by state (for simulation only)
    msetmp = zeros(1,S);
    if ~isempty(ThetasVec) && size(ThetasVec,1)==size(EstPara.mu1,1)
        % Group the Posterior ConnProbCom
        % PostConnProbCom: K*(K+1)/2 x T
        if ~isempty(real_states)
            [HMMStatesTmp, BestPerm] = ...
                permuteClasses(real_states(:),HMMStatesTmp(:));
            EstPara.mu1 = EstPara.mu1(:,BestPerm);
        end

        for s = 1:S
            msetmp(s) = norm((ThetasVec(:,s) - ...
                logistic(EstPara.mu1(:,s))),'fro');
        end
        
        if any(isnan(msetmp))
            hmm_flag = 0;
            warning('run_hmm: MSE is NaN');
        end
    end

        
    %%
    rcnt_old = rcnt;
    if EstPara.LL(end) > lmax || (rcnt==0)
        lmax = EstPara.LL(end);
        mse = msetmp;
        transmat = EstPara.transmat1;
        Mu = EstPara.mu1;
        Sigma = EstPara.Sigma1;
        HMMStates = HMMStatesTmp;
        KMStates = KMStatesTmp;
        rcnt = 1;
    else
        rcnt = rcnt + 1;
    end

    if verbose
        if first_print == 1
            fprintf('%d/%d',rcnt,nwait);
            first_print = 0;
        else
            fprintf([repmat('\b',1,...
                numel(num2str(rcnt_old))+numel(num2str(nwait))+1) '%d/%d'],...
                rcnt,nwait);
        end
    end
end % HMM Done.


if verbose, fprintf('\n'); end
end
