function [ModConn,NodeConn,AxisLab] = get_modular_probmat(CommEst,Mu,BestPerm,ncomm,stat,NodeName)

cdir = fileparts(mfilename('fullpath'));
cdir = strsplit(cdir,'/');
cdir = strjoin(cdir(1:end-1),'/');
addpath(strcat(cdir,"/external/dyn_sbm"));

logistic = @(x) 1./(1+exp(-x)); %logistic function, Real to [0,1]
[AxisLab,CommTab,CommFlag,nCommAll] = ...
    get_modular_axislab(CommEst,NodeName,ncomm);
%% 2. Calculate Mean or Median Modular Connectivity
[nnonzero,nstate] = size(Mu{1});
nsub = length(Mu);
m1 = zeros(nnonzero,nstate,nsub);
for s = 1:nsub
    for ss = 1:nstate
        if ~isempty(BestPerm) && numel(size(BestPerm))==2
            m1(:,ss,s) = Mu{s}(:,BestPerm(ss,s));
        elseif ~isempty(BestPerm)
            m1(:,ss,s) = Mu{s}(:,BestPerm(ss));
        else
            m1(:,ss,s) = Mu{s}(:,ss);
        end
    end
end
 
if stat == "mean"
    m1 = mean(m1,3);
elseif stat == "median"
    m1 = median(m1,3);
end

m1 = logistic(m1);
%% 3. Map from K x K to N x N
nsub = size(m1,3);

ModConn = zeros(nCommAll,nCommAll,nstate,nsub);
for sub = 1:nsub
    for s = 1:nstate
        ModConn(:,:,s,sub) = blockvec2mat(m1(:,s,sub));
    end
end
ModConn = ModConn(CommFlag,CommFlag,:,:);

NodeConn = {};
if nsub == 1
    NodeConnTmp = {};
    for s = 1:nstate
        NodeConn{s} = [];
        NodeConnTmp{s} = [];
    end

    for i = 1:ncomm
        nrow = CommTab(i,2);
        for j = 1:ncomm
            ncol = CommTab(j,2);
            for s = 1:nstate
                tmp = ones(nrow,ncol) .* ModConn(i,j,s);
                NodeConnTmp{s} = [NodeConnTmp{s} tmp];
            end
        end
    
        for s = 1:nstate
            NodeConn{s} = [NodeConn{s};NodeConnTmp{s}];
            NodeConnTmp{s} = [];
        end
    end
end
end