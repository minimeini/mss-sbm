function [hmmpath,EstPara,irun,kmpath,hmmflag] = ...
    class_hmm(data,S,transmat0,nmix,varargin)
% function [hmmpath,EstPara,irun,kmpath,hmmflag] = class_hmm(data,S,transmat0,nmix)
%
% Modified version of Project_SBM/MATLAB/Class_HMM
%
% Run until we get a valid HMM result, which satisfies
% - Not 'MATLAB:svd:matrixWithNaNInf' error in both initialization and
% optimization
% - EM algorithm converged, indicated by EstPara.converged == 1
% - Loglikelihood is finite
% - Number of inferred states is equal to the number of designated states
%
% >>> INPUT
%
% - data: nin x ntime numeric matrix, real valuded time-varing Theta
% - S: number of desinated states
% - transmat0: initial value of transition matrix
% - nmix: number of Gaussian mixtures for the underlying distribution
%
% >>> OUTPUT
% - hmmpath: 1 x ntime vector, state sequences estimated by HMM
% - EstPara: struct storing the HMM related estimates and diagnostics
% - irun: number of times of running EM algorithm till we can a valid HMM
% result
% - kmpath: 1 x ntime vector, state sequences estimated by K-Means, which
% is the initial value for the HMM estimates
% - hmmflag: logical, true is we successfully get a valid HMM result

cov_type =  'full';
if nargin > 4
    cov_type = varargin{1};
end
max_iter = 1000;

nin = size(data,1);
if nmix > 1
    mixmat0 = mk_stochastic(rand(S,nmix));
else
    mixmat0 = ones(S,1);
end

%% HMM
hmmflag = 0;
i = 0;
irun = [];
%%
while i < max_iter && ~hmmflag
    i = i + 1;
    try
        %%
        % 
        % Difference in `initialize_hmm` 
        % - support using a user-defined initial value. User-defined 
        % values should be provided in the fifth and sixth arguments, 
        % if used.
        [mu0, Sigma0, tmp] = initialize_hmm(nin,nmix,S,data,...
            [],[],cov_type);
        % tmp ntime x nstate
        

        prior0 = zeros(S,1);
        if any(tmp(:)==1)
            prior0(tmp(1,:)==1) = 1;
        else
            prior0(1) = 1;
        end
        
    catch err
        if strcmp(err.identifier, 'MATLAB:svd:matrixWithNaNInf')
            hmmflag = 0;
            disp(['WARNING - initialize_hmm: ' err.identifier]);
            continue;
        else
            rethrow(err);
        end
    end
    
    try
        EstPara = struct();
        [EstPara.LL, EstPara.prior1, EstPara.transmat1, EstPara.mu1, ...
            EstPara.Sigma1, EstPara.mixmat1, niter, EstPara.converged] = ...
            mhmm_em(data, prior0, transmat0, mu0, Sigma0, mixmat0, ...
            'max_iter', 15, 'cov_type', cov_type, ...
            'verbose', 0, 'adj_mix', 0);
    catch err
        if strcmp(err.identifier, 'MATLAB:svd:matrixWithNaNInf')
            hmmflag = 0;
            disp(['WARNING - mhmm_em: ' err.identifier]);
            continue;
        else
            warning('mixgauss_prob: Sigma not psd');
            rethrow(err);
        end
    end
    
    irun = [irun niter];
    

    B = mixgauss_prob(data, EstPara.mu1, EstPara.Sigma1, EstPara.mixmat1);
    [hmmpath] = viterbi_path(EstPara.prior1, EstPara.transmat1, B);
    nest = numel(unique(hmmpath(:)));

    
    if isinf(EstPara.LL(end)) || (nest < S) || ...
            (EstPara.converged==0) || isinf(EstPara.LL(1,1))
        continue;
    end
    
    hmmflag = 1;
end


kmpath = zeros(1,size(tmp,1));
try
for s = 1:S
    kmpath(tmp(:,s)==1) = s;
end
catch
end
end
