function transmat = get_default_transmat(nstate,inseq,diagval)
% function transmat = get_default_transmat(nstate)
%
% Get default transition matrix for HMM: self loop = 0.9, go to next state
% = 0.1.

if nargin < 2
    inseq = 1;
end

if nargin < 3
	diagval = 0.9;
end

if inseq == 1
    offval = 1 - diagval;
    tr = diag(diag(repmat(diagval,nstate))) + ...
        circshift(diag(diag(repmat(offval,nstate))),1,2);
else
    offval = (1-diagval) / (nstate-1);
    tr = diag(diag(repmat(diagval,nstate)));
    for i = 1:(nstate-1)
        tr = tr + circshift(diag(diag(repmat(offval,nstate))),i,2);
    end
end
transmat = mk_stochastic(tr);

end