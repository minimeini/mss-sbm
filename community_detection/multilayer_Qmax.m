function [S,Q] = multilayer_Qmax(gamma,omega,N,A)
T=size(A,3);

B=spalloc(N*T,N*T,(N+T)*N*T);
twomu=0;
for t=1:T
    k=sum(A(:,:,t));
    twom=sum(k);
    twomu=twomu+twom;
    indx=(1:N)+(t-1)*N;
    B(indx,indx)=A(:,:,t)-(k'*k)*gamma/twom;
end
twomu=twomu+T*omega*N*(T-1);
all2all = N*[(-T+1):-1,1:(T-1)];
B = B + omega*spdiags(ones(N*T,2*T-2),all2all,N*T,N*T);
[S,Q] = genlouvain(B,[],false);
Q = Q/twomu;

S = reshape(S,N,T);
S = multislice_pair_labeling(S);
