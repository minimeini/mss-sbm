function [S,Q] = singlelayer_Qmax(gamma, A) 

% function [S,Q,n_it] = SmallNet(gamma, A) 

k = full(sum(A));
twom = sum(k);
% B = full(A - gamma*k'*k/twom);
B = @(i) A(:,i) - gamma*k'*k(i)/twom;
[S,Q] = genlouvain(B,[],false);
% [S,Q] = genlouvain(B);
% [S,Q,n_it] = iterated_genlouvain(B);
Q = Q/twom;

